#!/usr/bin/python
# -*- coding: utf-8 -*-

#  pyregex.py
#
#  Copyright 2013 Calev Eliacheff <c.eliacheff@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

import string
import sys
import re
from collections import OrderedDict
from snippets import reg_snippets

try:
    from PySide.QtCore import *
    from PySide.QtGui import *
except:
    print("Error: This program needs PySide module.", file=sys.stderr)
    sys.exit(1)

class RegexGui(QMainWindow) :
    """ Main Windows """
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Regex")

        self.central = QWidget()
        self.search = Search()
        self.snippetList = RegList()
        layout = QHBoxLayout()
        layout.addWidget(self.search)
        layout.addWidget(self.snippetList)

        self.snippetList.list.itemActivated[QListWidgetItem].connect(self.search.insertSnippet)

        self.central.setLayout(layout)

        self.setCentralWidget(self.central)
        self.search.setFocus()

        self.show()

class Search(QWidget):
    """ Search and highlight text widget """

    def __init__(self, parent=None):
        super().__init__(parent)

        self.actions()
        self.nmatch = 0
        self.compiled = None

        #~ self.setGeometry(100, 100, 500, 700)

        self.labelR = QLabel("Entrez une Regex Python :")

        self.entry = QLineEdit()
        self.entry.textChanged[str].connect(self.compile)

        opt_names = ["Multiline", "IgnoreCase", "Verbose", "Dotall", "Locale"]
        self.opt_buttons = OrderedDict([(name, QCheckBox(name, self)) for name in opt_names])

        hb = QHBoxLayout()
        for button in self.opt_buttons.values():
            button.setCheckable(True)
            button.stateChanged.connect(self.compile)
            hb.addWidget(button)

        #~ Remplace
        self.labelReplace = QLabel("Remplacer par")
        self.replaceEntry = QLineEdit()
        self.replaceButton = QPushButton("Remplacer")
        self.replaceButton.clicked.connect(self.replace)
        hbReplace = QHBoxLayout()
        hbReplace.addWidget(self.labelReplace)
        hbReplace.addWidget(self.replaceEntry)
        hbReplace.addWidget(self.replaceButton)

        self.labelT = QLabel("Entrez un texte")

        #~ Texte à analyser
        self.text = QTextEdit()
        self.text.textChanged.connect(self.textChanged)

        #~ Barre de messages
        self.labelG = QLabel("Messages")
        self.msg = QLineEdit()
        self.msg.setReadOnly(True)

        layout = QGridLayout()
        layout.addWidget(self.labelR, 0, 0)
        layout.addWidget(self.entry, 1, 0)
        layout.addLayout(hb, 2, 0)
        layout.addLayout(hbReplace, 3, 0)
        layout.addWidget(self.labelT, 4, 0)
        layout.addWidget(self.text, 6, 0)
        layout.addWidget(self.labelG, 7, 0)
        layout.addWidget(self.msg, 8, 0)

        self.setLayout(layout)
        self.entry.setFocus()

        self.show()

    def actions(self):
        self.quitAction = QAction("&Quit", self, triggered=self.close)

    def getOptions(self) :
        """ Get compilation options from check buttons """
        options = 0
        for x in self.opt_buttons:
            if self.opt_buttons[x].isChecked():
                options = options | eval("re."+ x.upper())
        return options

    def validEntry(self, bool) :
        """ Color the regex entry line """
        if bool :
            self.entry.setStyleSheet("background: white")
        else :
            self.entry.setStyleSheet("background: red")

    def compile(self):
        """ Compile and check regex """
        try :
            self.compiled = re.compile(self.entry.text(), self.getOptions())
            self.validEntry(True)
        except re.error as msg :
            self.msg.setText(str(msg))
            self.validEntry(False)
            self.compiled = None
            self.cleanMatchs()
        self.evaluate()

    def evaluate(self) :
        """ Color matches and display catched groups """

        try :
            self.compiled
        except:
            self.cleanMatchs()
            return

        if self.compiled == None:
            self.cleanMatchs()
            return

        #~ Avoid dat infinite loop when using setCharFormat
        self.text.blockSignals(True)

        text = self.text.toPlainText()

        #~ Clean prev search
        self.cleanMatchs()

        self.nmatch = 0

        #~ Find all occurences w/ positions
        iterator = self.compiled.finditer(text)

        #~ Highlight
        for match in iterator:
            if match.group(0) == "":
                continue
            start, end = match.span()

            fmtMatch = QTextCharFormat()
            fmtMatch.setBackground(QColor("yellow"))

            #~ Set tooltip with groups
            fmtMatch.setToolTip(self.displayToolTip(match))

            cursor = self.text.textCursor()
            cursor.setPosition(end)
            cursor.movePosition(QTextCursor.Left, QTextCursor.KeepAnchor, end - start)
            cursor.setCharFormat(fmtMatch)

            self.nmatch = self.nmatch + 1

        if self.nmatch == 0 :
            self.msg.setText("No matchs found")
        else :
            self.msg.setText(str(self.nmatch)+" match(s) found")

        self.text.blockSignals(False)

    def cleanMatchs(self):
        """ Clean all highlights created w/ format"""

        self.nmatch = 0

        fmtWhite = QTextCharFormat()
        fmtWhite.setBackground(QColor("white"))
        self.text.setCurrentCharFormat(fmtWhite)

        cursor = self.text.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.movePosition(QTextCursor.Start, QTextCursor.KeepAnchor)
        cursor.setCharFormat(fmtWhite)

    def textChanged(self) :
        """ Evaluate on text change """
        if self.compiled :
            self.evaluate()

    def displayToolTip(self, match) :
        """ Display an informative tooltip on every match """

        x = 0
        groups = ""

        for m in match.groups() :
            groups = groups+"  "+str(x)+" : "+str(m)+"\n"
            x = x + 1

        tooltip =   "Match : "+str(match.group(0))+"\n"+\
                    "Index : "+str(match.span()[0])+"\n"+\
                    "Lenght : "+str(match.span()[1] - match.span()[0])+"\n"+\
                    "Groups : "+str(len(match.groups()))+"\n"+\
                    groups
        return tooltip

    def insertSnippet(self, snippet):
        self.entry.blockSignals(True)
        self.entry.insert(snippet.text())
        self.entry.blockSignals(False)
        self.compile()

    def replace(self) :
        self.compile()
        if self.nmatch :
            text = self.text.toPlainText()
            repl = self.replaceEntry.text()
            if text and (self.compiled is not None):
                modifiedText = self.compiled.sub(repl, text)
                self.text.setPlainText(modifiedText)


class RegList(QWidget) :
    """ Common regex list + info """

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setGeometry(0, 0, 500, 700)

        self.label = QLabel("Liste :")

        self.list = QListWidget()
        self.list.setMinimumHeight(self.height()*2/3)
        self.list.currentItemChanged.connect(self.printInfo)
        self.createList()

        self.info = QTextEdit()

        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.list)
        layout.addWidget(self.info)
        self.setLayout(layout)

        self.show()

    def createList(self):
        self.dict_reg = OrderedDict([(snippet, info) for snippet, info in reg_snippets])
        for snippet in self.dict_reg.keys() :
            self.list.addItem(snippet)

    def printInfo(self) :
        self.info.setText(self.dict_reg[self.list.currentItem().text()])

if __name__ == '__main__':

    app = QApplication(sys.argv)
    window = RegexGui()
    sys.exit(app.exec_())
