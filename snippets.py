#!/usr/bin/python
# -*- coding: utf-8 -*-

#~ Part of pyregex project

#~ From Wikipedia, you can edit this as you want
#~ Use raw format 'cause backslashes
reg_snippets = [
        [r"[ ]", r"Spécification de classe de caractères (par ex. : [a-z] indique une lettre dans l’intervalle de a à z)"],
        [r".", r"La classe de caractère prédéfinie des caractères graphiques visibles ou blancs ou de contrôle (à l’exclusion des caractères de sauts de ligne)"],
        [r"*", r"Quantificateur pour zéro, une ou plusieurs occurrences de ce qui précède ; équivaut à {0,}"],
        [r"?", r"Quantificateur pour au plus une occurrence de ce qui précède ; équivaut à {0,1}"],
        [r"+", r"Quantificateur pour une ou plusieurs occurrences de ce qui précède ; équivaut à {1,}"],
        [r"|", r"Alternative: soit ce qui précède soit ce qui suit"],
        [r"( )", r"Délimiteurs de groupe (avec capture)"],
        [r"\autre", r"Un des caractères spéciaux définis ci-dessus, mais interprétés littéralement"],
        [r"\t", r"Tabulation horizontale"],
        [r"\n", r"Saut de ligne"],
        [r"\v", r"Tabulation verticale"],
        [r"\f", r"Saut de page"],
        [r"\r", r"Retour chariot"],
        [r"\ooo", r"Caractère littéral dont le code octal (entre 0 et 377, sur 1 à 3 chiffres) est ooo"],

        [r"\b", r"Condition vraie à la limite d’un mot (sauf dans une classe de caractères)"],

        [r"\B", r"Condition vraie sauf à la limite d’un mot, l’opposé de \b (non reconnu dans une classe de caractères)"],
        [r"\w", r"Un caractère lettre ou chiffre ; équivaut à [0-9a-zA-Z_] (n'équivaut pas à la classe [[:alnum:]] de POSIX)"],
        [r"\W", r"Un caractère ni lettre, ni chiffre, le complément de \w (équivaut à la classe [^[:alnum:]] de POSIX)"],
        [r"\s", r"Un caractère espace ; équivaut à[ \t\n\r\f] (équivaut à la classe [[:space:]] de POSIX)"],
        [r"\S", r"Un caractère non espace, le complément de \s (équivaut à la classe [^[:space:]] de POSIX)"],
        [r"\d", r"Un chiffre ; équivaut à [0-9] (équivaut à la classe [[:digit:]] de POSIX)"],
        [r"\D", r"Un caractère non-chiffre, le complément de \d (équivaut à la classe [^[:digit:]] de POSIX)"],
        [r"{m,n}", r"Quantificateur borné pour au moins m et au plus n occurrences de ce qui précède"]
        ]

def snippetsList() :
    return reg_snippets
